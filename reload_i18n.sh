#!/bin/bash
# A simple script to reload translations from github

cd /tmp
git clone git@bitbucket.org:jc-ascaldera/discourse-si.git
cp -rT discourse-si/ /var/www/discourse/
cp -f discourse-si/reload_i18n.sh /root/reload_i18n.sh
chmod +x /root/reload_i18n.sh
rm -rf discourse-si
cd /var/www/discourse
sudo -u discourse RAILS_ENV=production bundle exec rake assets:clobber assets:precompile
